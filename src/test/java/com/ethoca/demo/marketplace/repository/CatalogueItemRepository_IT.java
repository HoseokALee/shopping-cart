package com.ethoca.demo.marketplace.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.ethoca.demo.marketplace.entity.CatalogueItem;

@SpringBootTest
public class CatalogueItemRepository_IT {
	@Autowired
	CatalogueItemRepository itemRepository;
	
	@Test
	void testSaveAndFetch() {
		CatalogueItem item = itemRepository.save(new CatalogueItem("321421323", "Hat", "A cool hat"));
		CatalogueItem itemFromRepo = itemRepository.findById(item.getId()).orElse(null);
		System.out.println(itemFromRepo);
		assertNotNull(itemFromRepo);
		cleanUp();
	}
	
	@Test
	void prepareTestData() {
		itemRepository.save(new CatalogueItem("651651515", "Glasses", "A cool pair of glasses"));
		itemRepository.save(new CatalogueItem("481651532", "Necklace", "A shiny necklace"));
		itemRepository.save(new CatalogueItem("213987283", "Jeans", "A pair of jeans"));
		itemRepository.save(new CatalogueItem("129837823", "Tissue Box", "A box of tissues"));
		itemRepository.save(new CatalogueItem("123897142", "Smartphone", "A smartphone"));
		itemRepository.save(new CatalogueItem("593089234", "Tape", "Regular tape"));
		itemRepository.save(new CatalogueItem("328791232", "Glue", "Regular glue"));
		itemRepository.save(new CatalogueItem("230958932", "Computer", "A computer"));
		itemRepository.save(new CatalogueItem("195845031", "Mouse", "Not of the animal variety"));
		itemRepository.save(new CatalogueItem("609439432", "Mouse", "A toy mouse"));
		itemRepository.save(new CatalogueItem("672093443", "Mug", "Reads \"World's best boss\""));
		itemRepository.save(new CatalogueItem("750192304", "Headphones", "Loud."));
		itemRepository.save(new CatalogueItem("598032984", "Speakers", "Louder"));
		itemRepository.save(new CatalogueItem("960923843", "Guitar", "A guitar"));
		
	}
	
	@Test
	void cleanUp() {
		itemRepository.deleteAll();
		assertEquals(0, itemRepository.findAll().size());
	}
}
