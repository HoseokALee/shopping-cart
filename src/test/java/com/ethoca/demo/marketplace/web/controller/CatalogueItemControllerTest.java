package com.ethoca.demo.marketplace.web.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public class CatalogueItemControllerTest {
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void shouldAllowAnonymousUsersToGetAllItems() throws Exception {
		this.mockMvc.perform(get("/api/catalogue"))
			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(content().contentType("application/json"));
		this.mockMvc.perform(get("/api/some_other_url")).andDo(print()).andExpect(status().isForbidden());  /**TODO: Should be 401(Unauthorized)*/
	}
}
