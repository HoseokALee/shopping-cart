package com.ethoca.demo.marketplace.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ethoca.demo.marketplace.entity.CatalogueItem;
import com.ethoca.demo.marketplace.repository.CatalogueItemRepository;

@Service
public class CatalogueItemService {
    @Autowired
    private CatalogueItemRepository itemRepository;
    
    public List<CatalogueItem> findAllCatalogueItems() {
    	return itemRepository.findAll();
    }
}