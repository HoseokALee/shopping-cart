package com.ethoca.demo.marketplace.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.ethoca.demo.marketplace.entity.CatalogueItem;

public interface CatalogueItemRepository extends MongoRepository<CatalogueItem, ObjectId> {
    
}