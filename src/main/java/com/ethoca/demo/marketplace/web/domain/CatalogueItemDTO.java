package com.ethoca.demo.marketplace.web.domain;

import com.ethoca.demo.marketplace.entity.CatalogueItem;

public class CatalogueItemDTO {
	private String sku;
	private String name;
	private String description;
	
	public CatalogueItemDTO() {}
	
	public CatalogueItemDTO(CatalogueItem item) {
		this.sku = item.getSku();
		this.name = item.getName();
		this.description = item.getDescription();
	} 
	
	public CatalogueItemDTO(String id, String sku, String name, String description) {
		super();
		this.sku = sku;
		this.name = name;
		this.description = description;
	}
	
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
