package com.ethoca.demo.marketplace.web.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ethoca.demo.marketplace.service.CatalogueItemService;
import com.ethoca.demo.marketplace.web.domain.CatalogueItemDTO;

@RestController()
@RequestMapping("api/catalogue")
public class CatalogueItemController {
	@Resource
	CatalogueItemService catalogueItemService;
	
	@GetMapping()
	public List<CatalogueItemDTO> findAllCatalogueItems() {
		return catalogueItemService.findAllCatalogueItems()
				.stream()
				.map(item -> new CatalogueItemDTO(item))
				.collect(Collectors.toList());
	}

}
