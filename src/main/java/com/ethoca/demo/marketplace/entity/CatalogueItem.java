package com.ethoca.demo.marketplace.entity;

import java.io.Serializable;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class CatalogueItem implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 7105851452063554752L;
	@Id
    private ObjectId id;
    private String sku;
	private String name;
    private String description;
    
    public CatalogueItem() {}
    
    public CatalogueItem(String sku, String name, String description) {
    	this.sku = sku;
    	this.name = name;
    	this.description = description;
    }
    
    public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "CatalogueItem [id=" + id + ", sku=" + sku + ", name=" + name + ", description=" + description + "]";
	}
	
	
}
