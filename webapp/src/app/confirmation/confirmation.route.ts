import { Routes } from '@angular/router';
import { ConfirmationComponent } from './confirmation.component';

export const route: Routes = [
    {path: 'confirmation', component: ConfirmationComponent}
]
