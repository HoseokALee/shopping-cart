import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmationComponent } from './confirmation.component';
import { RouterModule } from '@angular/router';
import { route } from './confirmation.route';



@NgModule({
  declarations: [ConfirmationComponent],
  imports: [
    CommonModule,
    RouterModule.forRoot(route)
  ]
})
export class ConfirmationModule { }
