import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ShoppingCartService } from '../shopping-cart/shopping-cart.service';
import { ShoppingCartItem } from '../model/shopping-cart-item.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.css']
})
export class ConfirmationComponent implements OnInit, AfterViewInit {
  items: ShoppingCartItem[]

  constructor(
    private shoppingCartService: ShoppingCartService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.items = this.shoppingCartService.getCart();
  }

  ngAfterViewInit() {
    this.shoppingCartService.clearShoppingCart();
  }

  navigateToCatalogue() {
    this.router.navigateByUrl('/catalogue');
  }

}
