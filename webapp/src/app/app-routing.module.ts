import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShoppingCartModule } from './shopping-cart/shopping-cart.module';


const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: '/catalogue'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
