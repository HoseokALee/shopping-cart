import { Routes } from '@angular/router';
import { CheckoutComponent } from './checkout.component';

export const route: Routes = [
    {path: 'checkout', component: CheckoutComponent}
]