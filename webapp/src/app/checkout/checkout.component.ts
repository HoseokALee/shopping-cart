import { Component, OnInit } from '@angular/core';
import { ShoppingCartService } from '../shopping-cart/shopping-cart.service';
import { ShoppingCartItem } from '../model/shopping-cart-item.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  shoppingCart: ShoppingCartItem[];
  isEditEnabled = false;
  
  constructor(
    private shoppingCartService: ShoppingCartService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.shoppingCart = this.shoppingCartService.getCart();
    this.shoppingCartService.shoppingCartEvent.subscribe((shoppingCartEvent) => {
      this.shoppingCart = shoppingCartEvent;
    })
  }

  navigateToCatalogue() {
    this.router.navigateByUrl('/catalogue');
  }

  removeItemFromCart(sku: string) {
    this.shoppingCartService.removeItemFromCart(sku);
  }

  enableEditButton() {
    this.isEditEnabled = true;
  }

  updateCart() {
    this.shoppingCartService.saveCart(this.shoppingCart);
    this.isEditEnabled = false;
  }

  confirmPurchase() {
    this.router.navigateByUrl('/confirmation');
  }

}
