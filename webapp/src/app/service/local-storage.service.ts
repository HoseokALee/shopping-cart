import { Injectable } from '@angular/core';
import * as store from 'store2' ;

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }

  saveToLocalStorage(key: string, value: any, overwrite = false) {
    return store.set(key, value, overwrite);
  }

  retrieveFromLocalStorage(key: string): any {
    return store.get(key);
  }

  clearLocalStorage() {
    store.clearAll();
  }

  clearStorageAtKey(key: string) {
    store.remove(key);
  }
}
