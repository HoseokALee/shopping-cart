import { TestBed } from '@angular/core/testing';

import { LocalStorageService } from './local-storage.service';

describe('LocalStorageService', () => {
  let service: LocalStorageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LocalStorageService);
    service.clearLocalStorage();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  
  it('should store data to localStorage and be able to retrieve it', () => {
    service.saveToLocalStorage('foo', 'bar');
    const value = service.retrieveFromLocalStorage('foo');
    const unsavedValue = service.retrieveFromLocalStorage('novalue')
    expect(value).toBeDefined();
    expect(value).toBe('bar');
    expect(unsavedValue).toBeDefined()
  });

  it('should not overwrite with overwrite == false', () => {
    service.saveToLocalStorage('foo', 'bar');
    service.saveToLocalStorage('foo', 'unsaved val');
    const value = service.retrieveFromLocalStorage('foo');
    expect(value).toBeDefined();
    expect(value).toBe('bar');
  });

  it('should overwrite with overwrite == true', () => {
    service.saveToLocalStorage('overwritethis', 'bar');
    let value = service.retrieveFromLocalStorage('overwritethis');
    expect(value).toBeDefined();
    expect(value).toBe('bar');
    service.saveToLocalStorage('overwritethis', 'saved val', true);
    value = service.retrieveFromLocalStorage('overwritethis');
    expect(value).toBeDefined();
    expect(value).toBe('saved val');
  });

});
