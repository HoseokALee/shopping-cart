import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CatalogueModule } from './catalogue/catalogue.module';
import { HttpClientModule } from '@angular/common/http';
import { CheckoutModule } from './checkout/checkout.module';
import { ConfirmationModule } from './confirmation/confirmation.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CatalogueModule,
    HttpClientModule,
    CheckoutModule,
    ConfirmationModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
