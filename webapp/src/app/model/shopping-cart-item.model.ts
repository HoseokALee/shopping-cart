import { CatalogueItem } from './catalogue-item.model';

export interface ShoppingCartItem {
    item: CatalogueItem;
    quantity: number;
}