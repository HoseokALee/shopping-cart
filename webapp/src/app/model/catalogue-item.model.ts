export interface CatalogueItem {
    sku: string;
    name: string;
    description: string;
}