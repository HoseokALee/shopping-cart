import { Component, OnInit } from '@angular/core';
import { ShoppingCartService } from './shopping-cart.service';
import { ShoppingCartItem } from '../model/shopping-cart-item.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {
  shoppingCart: ShoppingCartItem[];

  constructor(
    private shoppingCartService: ShoppingCartService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.shoppingCart = this.shoppingCartService.getCart();
    this.shoppingCartService.shoppingCartEvent.subscribe((cart) => {
      this.shoppingCart = cart;
    });
  }

  navigateToCheckout() {
    this.router.navigateByUrl('/checkout')
  }

}
