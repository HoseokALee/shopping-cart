import { Injectable } from '@angular/core';
import { LocalStorageService } from '../service/local-storage.service';
import { ShoppingCartItem } from '../model/shopping-cart-item.model';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {
  shoppingCartEvent = new Subject<ShoppingCartItem[]>();

  constructor(private localStorageService: LocalStorageService) {
    this.localStorageService.saveToLocalStorage('cart', []);
  }

  addItemToCart(item: ShoppingCartItem) {
    let cart: ShoppingCartItem[] = this.localStorageService.retrieveFromLocalStorage('cart');
    if (!cart) {
      cart = [];
    }
    const existingItem = cart.findIndex((cartItem) => {return cartItem.item.sku === item.item.sku});
    if (existingItem !== -1) {
      cart[existingItem].quantity += item.quantity;
    } else {
      cart.push(item);
    }
    this.saveCart(cart);
  }
  
  getCart(): ShoppingCartItem[] {
    return this.localStorageService.retrieveFromLocalStorage('cart');
  }
  
  saveCart(cart: ShoppingCartItem[]) {
    cart = cart.filter((cartItem) => {
      return cartItem.quantity > 0;
    });
    this.localStorageService.saveToLocalStorage('cart', cart, true);
    cart = this.getCart();
    this.shoppingCartEvent.next(cart);
  }

  clearShoppingCart() {
    this.localStorageService.saveToLocalStorage('cart', [], true);
  }

  removeItemFromCart(sku: string) {
    let cart: ShoppingCartItem[] = this.localStorageService.retrieveFromLocalStorage('cart');
    if (cart) {
      cart = cart.filter((cartItem) => 
        {return cartItem.item.sku !== sku}
      );
      this.saveCart(cart);
    }
  }
}
