import { TestBed } from '@angular/core/testing';

import { ShoppingCartService } from './shopping-cart.service';
import { ShoppingCartItem } from '../model/shopping-cart-item.model';
import { CatalogueItem } from '../model/catalogue-item.model';

describe('ShoppingCartService', () => {
  let service: ShoppingCartService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ShoppingCartService);
    service.clearShoppingCart();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return the cart when retrieved', () => {
    expect(service.getCart()).toBeDefined();
    expect(service.getCart()).toEqual([]);
  });

  it('should consolidate items if item already exists in cart', () => {
    expect(service.getCart()).toBeDefined();
    expect(service.getCart()).toEqual([]);
    const item: ShoppingCartItem = {item: {sku: '123'} as CatalogueItem, quantity: 3};
    service.addItemToCart(item);
    expect(service.getCart()[0].quantity).toEqual(3);
    service.addItemToCart(item);
    expect(service.getCart().length).toBe(1);
    expect(service.getCart()[0].quantity).toEqual(6);
  });
});
