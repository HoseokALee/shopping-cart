import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CatalogueListComponent } from './catalogue-list/catalogue-list.component';
import { RouterModule } from '@angular/router';
import { catalogueRoutes } from './catalogue.route';
import { ShoppingCartModule } from '../shopping-cart/shopping-cart.module';

@NgModule({
  declarations: [CatalogueListComponent],
  imports: [
    CommonModule,
    ShoppingCartModule,
    RouterModule.forRoot(catalogueRoutes)
  ]
})
export class CatalogueModule { }
