import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { CatalogueItem } from '../model/catalogue-item.model'

@Injectable({
  providedIn: 'root'
})
export class CatalogueService {

  constructor(private http: HttpClient) { }

  getEntireCatalogue(): Observable<CatalogueItem[]> {
    return this.http.get<CatalogueItem[]>("/api/catalogue");
  }
}
