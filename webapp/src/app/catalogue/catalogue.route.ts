import { Routes } from '@angular/router';
import { CatalogueListComponent } from './catalogue-list/catalogue-list.component';

export const catalogueRoutes: Routes = [
    {path: 'catalogue', component: CatalogueListComponent}
]