import { Component, OnInit } from '@angular/core';
import { CatalogueService } from '../catalogue.service';
import { Observable } from 'rxjs';
import { CatalogueItem } from 'src/app/model/catalogue-item.model';
import { ShoppingCartItem } from 'src/app/model/shopping-cart-item.model';
import { ShoppingCartService } from 'src/app/shopping-cart/shopping-cart.service';

@Component({
  selector: 'app-catalogue-list',
  templateUrl: './catalogue-list.component.html',
  styleUrls: ['./catalogue-list.component.css']
})
export class CatalogueListComponent implements OnInit {

  catalogueItems: Observable<CatalogueItem[]>;

  constructor(
    private catalogueService: CatalogueService,
    private shoppingCartService: ShoppingCartService
  ) { }

  ngOnInit(): void {
    this.catalogueItems = this.catalogueService.getEntireCatalogue();
  }

  addItemToCart(item: CatalogueItem, quantity: number) {
    if (!quantity) {
      quantity = 1;
    }
    const shoppingCartItem = {item, quantity} as ShoppingCartItem;
    this.shoppingCartService.addItemToCart(shoppingCartItem);
  }

}
