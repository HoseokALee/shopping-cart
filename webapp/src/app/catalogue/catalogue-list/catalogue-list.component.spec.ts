import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogueListComponent } from './catalogue-list.component';
import { CatalogueService } from '../catalogue.service';
import { of } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('CatalogueListComponent', () => {
  let component: CatalogueListComponent;
  let fixture: ComponentFixture<CatalogueListComponent>;
  let catalogueService: CatalogueService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ CatalogueListComponent ],
      providers: [CatalogueService]
    })
    .compileComponents();
    catalogueService = TestBed.get(CatalogueService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogueListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get all of the items on init', () => {
    component.ngOnInit();
    spyOn(catalogueService, 'getEntireCatalogue').and.callFake(() => of([]));
    fixture.detectChanges();
    expect(component.catalogueItems).toBeTruthy();
  });
});
